clear 
sExpName = 'E04'; %%%Experiment of Interest is E04 
sFileName = [sExpName '.mat'];  %%%%Define Save Destination and File Name 

if exist(sFileName,'file')  %%%Check if data already Downloaded, otherwise proceed to data aquisition.
    load(sFileName);
else
    oExperiment = MDBExperiment(sExpName); %%%Construct MDBExperiment object. 
    save(sFileName, 'oExperiment');
end

%%%Define Monkeys of Interest. 

sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cName = horzcat(sSevere,sMild); 
%%%%%%%%E04 MDBExperiment Object have 8 subject, the following for loop
%%%%%%%%Finds the index of the 4 objects of interest. 
for i = 1:2 
    for j = 1:8
        if strncmp(sSevere{i},oExperiment.Subject{j}.Name(5:7),3) == 1 
            mIdxMonkeys(i) = j;
        end
        if strncmp(sMild{i},oExperiment.Subject{j}.Name(5:7),3) == 1 
            mIdxMonkeys(i+2) = j;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Data Aquisition 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Data Types
%    'Clinical'
%    'Functional Genomics'
%    'Immunology'
%    'Lipidomics'
%    'Metabolomics'

sFileName = [sExpName 'CompleteData.mat'];%%%%Define Save Destination and File Name 
cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
cDataLables = {}; 
if exist(sFileName,'file') %%%Check if data already Downloaded, otherwise proceed to data aquisition.
    load(sFileName) 
else 
    %%%%%%%%%% Get INNATE IMMUNE DATA%%%%%%%%%%%%%%%%%
    
    oExplorer = MDBAnalysis();  %%%Construct oExplorer as MDBAnalysis Object 
    for k = 1:length(cDataTypes)
        nCol = size(oExplorer.Data,2);
        for i =1:length(mIdxMonkeys) %%%For loop to get MDB data object for each Subject
                x = oExperiment.Subject{mIdxMonkeys(i)};  %%%X is the MDBSubject object of a subejct
                cCount = nCol+1; 
                for j = 1:size(x.Data(cDataTypes{k}).DataAssociations,1)
                    temp = x.Data(cDataTypes{k}).DataAssociations{j,1};
                    if k ~= 3
                        cMetaData = x.Data(cDataTypes{k}).DataAssociations(j,:);
                        T = x.Data(cDataTypes{k}).getLowFreqTimeSeries(cMetaData);
                        oExplorer.Data{i,cCount} = T; %%%Append to oExplorer.Data
                        cCount = cCount + 1; 
                    elseif temp == 70 || temp == 69
                        cMetaData = x.Data(cDataTypes{k}).DataAssociations(j,:);
                        T = x.Data(cDataTypes{k}).getLowFreqTimeSeries(cMetaData);
                        oExplorer.Data{i,cCount} = T; %%%Append to oExplorer.Data
                        cCount = cCount + 1; 
                    end
                end
        end
    end
            save(sFileName,'oExplorer');

end
