clear 
sExpName = 'E04'; %%%Experiment of Interest is E04 
sFileName = [sExpName '.mat'];  %%%%Define Save Destination and File Name 

if exist(sFileName,'file')  %%%Check if data already Downloaded, otherwise proceed to data aquisition.
    load(sFileName);
else
    oExperiment = MDBExperiment(sExpName); %%%Construct MDBExperiment object. 
    save(sFileName, 'oExperiment');
end

%%%Define Monkeys of Interest. 

sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cName = horzcat(sSevere,sMild); 
%%%%%%%%E04 MDBExperiment Object have 8 subject, the following for loop
%%%%%%%%Finds the index of the 4 objects of interest. 
for i = 1:2 
    for j = 1:8
        if strncmp(sSevere{i},oExperiment.Subject{j}.Name(5:7),3) == 1 
            mIdxMonkeys(i) = j;
        end
        if strncmp(sMild{i},oExperiment.Subject{j}.Name(5:7),3) == 1 
            mIdxMonkeys(i+2) = j;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Data Aquisition 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Data Types
%    'Clinical'
%    'Functional Genomics'
%    'Immunology'
%    'Lipidomics'
%    'Metabolomics'

sFileName = [sExpName 'CompleteData.mat'];%%%%Define Save Destination and File Name 
cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
cDataLables = {}; 
if exist(sFileName,'file') %%%Check if data already Downloaded, otherwise proceed to data aquisition.
    load(sFileName) 
else 
    %%%%%%%%%% Get INNATE IMMUNE DATA%%%%%%%%%%%%%%%%%
    
    oExplorer = MDBAnalysis();  %%%Construct oExplorer as MDBAnalysis Object 
    for k = 1:length(cDataTypes)
        nCol = size(oExplorer.Data,2);
        for i =1:length(mIdxMonkeys) %%%For loop to get MDB data object for each Subject
                x = oExperiment.Subject{mIdxMonkeys(i)};  %%%X is the MDBSubject object of a subejct
                cCount = nCol+1; 
                for j = 1:size(x.Data(cDataTypes{k}).DataAssociations,1)
                    temp = x.Data(cDataTypes{k}).DataAssociations{j,1};
                    if k ~= 3
                        cMetaData = x.Data(cDataTypes{k}).DataAssociations(j,:);
                        T = x.Data(cDataTypes{k}).getLowFreqTimeSeries(cMetaData);
                        oExplorer.Data{i,cCount} = T; %%%Append to oExplorer.Data
                        cCount = cCount + 1; 
                    elseif temp == 70 || temp == 69
                        cMetaData = x.Data(cDataTypes{k}).DataAssociations(j,:);
                        T = x.Data(cDataTypes{k}).getLowFreqTimeSeries(cMetaData);
                        oExplorer.Data{i,cCount} = T; %%%Append to oExplorer.Data
                        cCount = cCount + 1; 
                    end
                end
        end
    end
            save(sFileName,'oExplorer');

end

%%%%%%%%%%%%Filter FXGN Data%%%%%%%%%%%%%%%%%%%%

mIdxFXGN = oExplorer.findDataType('Functional Genomics');  %%%%find the index of FXGN data within oExplorer
%%%%Filter out genes that have expression <= 2.5 that have occurence > 0. 

oExplorer = oExplorer.filterTimeSeries(mIdxFXGN,2.5,0); 


%mIdxMETA = oExplorer.findDataType('METABOLOMIC'); %%%%find the index of METABOLOMIC data within oExplorer

%%%%Filter out Metabolites that have expression <= 0 that have occurence > 24. 

%oExplorer = oExplorer.filterTimeSeries(mIdxMETA,0,24);

%%%Time series within oExplorere are normalized to have mean = 0 and variance 1; 
oExplorer = setMeanVarTimeSeries(oExplorer,[]); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%                 Immune Clinical Analysis %%%%%%%%%
%%%%
%%%%
%%%%%Generate new instance of MDB Analysis 

oExplorerAggregated = MDBAnalysis(); 

%%%%%For Each Monkey, Aggregate datatype of interest. 

mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxImmune(i,:) mIdxClinical(i,:)]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

g1 = [1 2]; %%%Index of Severe Monkeys RME RfA 
g2 = [3 4]; %%%Index of Mild Monkeys RIC RSB

display('Whole Blood Clinical Immune Analysis');
%%%%%%%%%%%% Run MPATS %%%%%%%%%%%%%%%%%%%%%%%

if exist('E04WBClinImmuneMPATS.mat','file') ==0
    
strucMPATS = oExplorerAggregated.MPATS(g1,g2,500); %%%Conduct MPATS Analysis using parameter 500. 

save('E04WBClinImmuneMPATS.mat','strucMPATS'); 
clear strucMPATS;

end
%%%%%%%%%%%% Run Differential Correlation %%%%%%%%%%%%%


%%%Conduct Differential Correlation Analysis using spearman's correlation. 
if exist('E04WBClinImmuneDiffCorr.mat','file') ==0

strucDiffCorr = oExplorerAggregated.DiffCorr(g1,g2,'spearman');

save('E04WBClinImmuneDiffCorr.mat','strucDiffCorr'); 

clear strucDiffCorr;

end
%%%Conduct Differential Correlation Analysis using pearson's correlation. 

if exist('E04WBClinImmuneDiffCorr2.mat','file') ==0

strucDiffCorr2 = oExplorerAggregated.DiffCorr(g1,g2,'pearson'); 

save('E04WBClinImmuneDiffCorr2.mat','strucDiffCorr2'); 

clear strucDiffCorr2;

end
%%%%%%%%Create Discretized version of data, discretizing all 4 subjects and
%%%%%%%specifying maximum discretization level of 4. 

if exist('E04WBClinImmuneChiSQ.mat','file') ==0

oExplorerDiscretized = oExplorerAggregated.discretizeByGroup({[1 2 3 4]},6);

%%%%%%%%%%%% Run Test of Independence On Discretized Data %%%%%%%%%%%%%%%%%%%

strucChiSQ = oExplorerDiscretized.Chi2(g1,g2,'Pearson'); 
save('E04WBClinImmuneChiSQ.mat','strucChiSQ'); 

clear strucChiSQ;

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%                 FXGN Immune Clinical Analysis %%%%%%%%%
%%%%
%%%%
%%%%%Generate new instance of MDB Analysis 

sDate = '2013-11-20 00:00:00.0'; 
for i = 1:3
    Idx2 = find(strcmp(sDate,oExplorer.Data{i,18}.DataPrimitive.Time));
    n = length(oExplorer.Data{i,18}.DataPrimitive.Time); 
    temp = 1:n ~= Idx2; 
    oExplorer.Data{i,18}.DataPrimitive.Time = oExplorer.Data{i,18}.DataPrimitive.Time(temp);
    oExplorer.Data{i,18}.DataPrimitive.Table = oExplorer.Data{i,18}.DataPrimitive.Table(:,temp);
end

oExplorerAggregated = MDBAnalysis(); 

%%%%%For Each Monkey, Aggregate datatype of interest. 

mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxImmune(i,:) mIdxClinical(i,:) 18]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

g1 = [1 2]; %%%Index of Severe Monkeys RME RfA 
g2 = [3 4]; %%%Index of Mild Monkeys RIC RSB

display('Whole Blood Clinical Immune FXGN Analysis');
%%%%%%%%%%%% Run MPATS %%%%%%%%%%%%%%%%%%%%%%%
if exist('E04WBFXGNClinImmuneMPATS.mat','file') ==0

    strucMPATS = oExplorerAggregated.MPATS(g1,g2,500); %%%Conduct MPATS Analysis using parameter 500. 

    save('E04WBFXGNClinImmuneMPATS.mat','strucMPATS'); 
    clear strucMPATS;
end
%%%%%%%%%%%% Run Differential Correlation %%%%%%%%%%%%%


%%%Conduct Differential Correlation Analysis using spearman's correlation. 

if exist('E04WBFXGNClinImmuneDiffCorr.mat','file') ==0

    strucDiffCorr = oExplorerAggregated.DiffCorr(g1,g2,'spearman');

    save('E04WBFXGNClinImmuneDiffCorr.mat','strucDiffCorr'); 

    clear strucDiffCorr;
end
%%%Conduct Differential Correlation Analysis using pearson's correlation. 

if exist('E04WBFXGNClinImmuneDiffCorr2.mat','file') ==0

    strucDiffCorr2 = oExplorerAggregated.DiffCorr(g1,g2,'pearson'); 

    save('E04WBFXGNClinImmuneDiffCorr2.mat','strucDiffCorr2'); 

    clear strucDiffCorr2;
end
%%%%%%%%Create Discretized version of data, discretizing all 4 subjects and
%%%%%%%specifying maximum discretization level of 4. 

oExplorerDiscretized = oExplorerAggregated.discretizeByGroup({[1 2 3 4]},6);

%%%%%%%%%%%% Run Test of Independence On Discretized Data %%%%%%%%%%%%%%%%%%%
if exist('E04WBFXGNClinImmuneChiSQ.mat','file') ==0

    strucChiSQ = oExplorerDiscretized.Chi2(g1,g2,'Pearson'); 
    save('E04WBFXGNClinImmuneChiSQ.mat','strucChiSQ'); 

    clear strucChiSQ;
end