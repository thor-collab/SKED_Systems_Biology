clear
addpath('../function');
addpath('../xMDBOracleClasses');
load('E04E04CompleteDataV2.mat');
sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};

sName = [sSevere sMild];
tempName = {}; 
for i = 1:8 
    if isempty(oExplorer.Data{i,1}) == 0
        tempName{i} = oExplorer.Data{i,1}.Name(end-4:end-2);
    else
        tempName{i} = '_';
    end
end
for i = 1:4
    temp = strcmp(sName{i},tempName);
    mIdx(i) = find(temp); 
end 

oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(mIdx(i),6);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mParasiteIdx = strncmp(oExplorerAggregated.Data{1}.DataPrimitive.VarNames,'x_PCYB',6);
mParasiteIdx = mParasiteIdx == 0; 
cGeneNames = oExplorerAggregated.Data{1}.DataPrimitive.VarNames(mParasiteIdx);
counts = []; 

for i = 1:4
    temp2 = datenum(oExplorerAggregated.Data{i}.DataPrimitive.Time);
    [a b1] = sort(temp2,'ascend');
    counts(:,i) = oExplorerAggregated.Data{i}.DataPrimitive.Table(mParasiteIdx,b1(1)); 
end 

%%%%%%%%%%%%%Lib Size Normalization%%%%%%%%%%%%%%%%%

% estimate pseudo-reference with geometric mean row by row
pseudoRefSample = geomean(counts,2);
nz = pseudoRefSample > 0;
ratios = bsxfun(@rdivide,counts(nz,:),pseudoRefSample(nz));
sizeFactors = median(ratios,1);

% transform to common scale
normCounts = bsxfun(@rdivide,counts,sizeFactors);


qReport = []; 
log2SevereUpFCReport = [];
log2MildUpFCReport = [];
pReport = []; 

    severe = [1 2];
    mild = [3 4]; 
    temp1 = mean(normCounts(:,severe),2); 
    temp2 = mean(normCounts(:,mild),2); 
    
    a = temp1./temp2; 
    b = temp2./temp1; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tLocal = nbintest(normCounts(:,severe),normCounts(:,mild),'VarianceLink','LocalRegression'); 
    [adjPval q] = mafdr(tLocal.pValue);
    qReport= q; 
    pReport = tLocal.pValue;
    log2SevereUpFCReport = a;
    log2MildUpFCReport = b;

    temp = normCounts < 10; 
    temp = sum(temp,2); 
    idx = find(temp==0); 
    
    pVal = pReport(idx); 
    [temp qVal] = mafdr(pVal); 
    FoldChange = log2SevereUpFCReport(idx);
    FilteredGeneName = cGeneNames(idx);
    
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    scatter(log2(temp1(idx)),log2(log2SevereUpFCReport(idx)),3,temp,'o');
    colormap(flipud(cool(256)))
    colorbar;
    ylabel('log2(Fold Change)')
    xlabel('log2(Mean of normalized counts)')
    title('Fold change by FDR')
    set(gca,'FontSize',20)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename = 'T1SevereMildSignificant.xlsx';
    cReport = {}; 
    idx = qVal <=0.05; 
    qVal = qVal(idx);
    FoldChange = FoldChange(idx);
    FilteredGeneName = FilteredGeneName(idx); 
    [a b] = sort(FoldChange,'descend');
    for i = 1:size(FilteredGeneName,1)
        cReport{i,1} = FilteredGeneName{b(i)}(3:end-1);
        cReport{i,2} = FoldChange(b(i));
    end 
    xlswrite(filename,cReport)

