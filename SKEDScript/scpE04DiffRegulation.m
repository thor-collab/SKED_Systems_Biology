clear 

[numT2MildSevere txt rawT2MildSevere] = xlsread('DifferentialExpression/T2SevereMildSignificant.xlsx');
[numT2T1Mild txt rawT2T1Mild] = xlsread('DifferentialExpression/T2T1MildSignificant.xlsx');
[numT2T1Severe txt rawT2T1Severe] = xlsread('DifferentialExpression/T2T1SevereSignificant.xlsx');

T2SevereUp = rawT2MildSevere(numT2MildSevere>1,1);
FC = numT2MildSevere(numT2MildSevere>1);
T1T2SevereUp = rawT2T1Severe(numT2T1Severe>1,1);

[SevereDiffReg ia ib] = intersect(T2SevereUp,T1T2SevereUp);
FC = FC(ia); 
[a b] = sort(FC,'descend'); 

temp1 = {};

for i = 1:length(b)
        temp1{i,1} = SevereDiffReg{b(i)};
        temp1{i,2} = num2str(FC(b(i)));
end

fileName = 'DifferentialExpression/T2SevereUp.xlsx';
    xlswrite(fileName,temp1)


T2MildUp = rawT2MildSevere(numT2MildSevere<1,1);
FC = numT2MildSevere(numT2MildSevere<1);
T1T2MildUp = rawT2T1Mild(numT2T1Mild>1,1);

[SevereDiffReg ia ib] = intersect(T2MildUp,T1T2MildUp);
FC = FC(ia); 
FC = 1./FC;
[a b] = sort(FC,'descend'); 
temp1 = {};

for i = 1:length(b)
        temp1{i,1} = SevereDiffReg{b(i)};
        temp1{i,2} = num2str(FC(b(i)));
end

fileName = 'DifferentialExpression/T2MildUp.xlsx';
    xlswrite(fileName,temp1)


T2SevereDown = rawT2MildSevere(numT2MildSevere<1,1);
FC = numT2MildSevere(numT2MildSevere<1);
T1T2SevereDown = rawT2T1Severe(numT2T1Severe<1,1);

[SevereDiffReg ia ib] = intersect(T2SevereDown,T1T2SevereDown);
FC = FC(ia); 
FC = 1./FC;
[a b] = sort(FC,'descend'); 
temp1 = {};

for i = 1:length(b)
        temp1{i,1} = SevereDiffReg{b(i)};
        temp1{i,2} = num2str(FC(b(i)));
end

fileName = 'DifferentialExpression/T2SevereDown.xlsx';
    xlswrite(fileName,temp1)



T2MildDown = rawT2MildSevere(numT2MildSevere>1,1);
FC = numT2MildSevere(numT2MildSevere>1);
T1T2MildDown = rawT2T1Mild(numT2T1Mild < 1 ,1);

[SevereDiffReg ia ib] = intersect(T2MildDown,T1T2MildDown);
FC = FC(ia); 
[a b] = sort(FC,'descend'); 
temp1 = {};

for i = 1:length(b)
        temp1{i,1} = SevereDiffReg{b(i)};
        temp1{i,2} = num2str(FC(b(i)));
end

fileName = 'DifferentialExpression/T2MildDown.xlsx';
    xlswrite(fileName,temp1)




