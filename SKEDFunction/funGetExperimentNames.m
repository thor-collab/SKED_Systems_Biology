function cExperiments = funGetExperimentNames(cStructures)
    for i = 1:length(cStructures)
        cExperiments{i} = cStructures{i}.Data{2,1}.ExperimentID;
    end
end