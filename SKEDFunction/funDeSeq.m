function tReport = funDeSeq(strucLibSizeNormalizedData,g1,g2)
    
    mG1 = [];
    mG2 = [];
    for i = 1:length(g1)
        if isempty(g1{i}) == 0
                mG1 = [mG1 strucLibSizeNormalizedData.cRawCounts{i}(:,g1{i})];
        end
    end
    
    for i = 1:length(g2)
        if isempty(g2{i}) == 0
                mG2 = [mG2 strucLibSizeNormalizedData.cRawCounts{i}(:,g2{i})];
        end
    end
        tLocal = nbintest(mG1,mG2,'VarianceLink','LocalRegression'); 
        pValue = tLocal.pValue;
        [mFDR QValue] = mafdr(pValue); 
        FoldChange = mean(mG1,2)./mean(mG2,2);
        GeneNames = strucLibSizeNormalizedData.VarNames{1}; 
        tReport = table(GeneNames,pValue,FoldChange); 
        
        figure('units','normalized','outerposition',[0 0 1 1]);
        histogram(tLocal.pValue,100)
        title('Histogram of P Values');
        
        figure('units','normalized','outerposition',[0 0 1 1]);
        scatter(log2(mean(mG1,2)),log2(FoldChange),3,QValue,'o')
        colormap(flipud(cool(256)))
        colorbar;
        ylabel('log2(Fold Change)')
        xlabel('log2(Mean of normalized counts)')
        title('Fold change by FDR')

end