function strucData = funCombineData(varargin)

    cVarName = varargin{1}.VarNames{1};
    for j = 1:nargin
        for i = 1:size(varargin{j}.VarNames,2)
            cVarName = intersect(varargin{j}.VarNames{i},cVarName);
        end
    end
    c = 1; 
    
    for j = 1:nargin
        strucData1 = varargin{j};
        for i = 1:size(strucData1.VarNames,2)
            strucData.VarNames{c} = cVarName;
            [temp ia ib] = intersect(strucData1.VarNames{i},cVarName);
            strucData.cSubject{c} = strucData1.cSubject{i};
            strucData.cTime{c} = strucData1.cTime{i};
            strucData.cRawCounts{c} = strucData1.cRawCounts{i}(ia,:);
            strucData.Experiment{c} = strucData1.Experiment{i};
            c = c+1; 
        end 
    end
    
end