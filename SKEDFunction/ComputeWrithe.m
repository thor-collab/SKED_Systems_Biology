function w=ComputeWrithe(xim1,xi,xjm1,xj)
a=xjm1-xim1;
b=xjm1-xi;
c=xj-xi;
d=xj-xim1;
x=xi-xim1;
y=xj-xjm1;
w=area(a,b,c,d,x,y);
end
function A=area(a,b,c,d,x,y)
i=normalizer(a);
j=normalizer(b);
k=normalizer(c);
l=normalizer(d);
edgei=normalizer(x);
edgej=normalizer(y);
s=sign(-sum(conj(i) .*cross(edgei,edgej)));
%angle 1
if sum(normalizer(cross(i,l)) .*normalizer(cross(l,k))) <-1
    A1=acos(-1);
else
        if sum(conj(normalizer(cross(i,l)) .*normalizer(cross(l,k))))>1
            A1=0;
        else
            A1=acos(sum(conj(normalizer(cross(i,l)) .*normalizer(cross(l,k)))));
    
        end
end
%angle 2
        if sum(normalizer(cross(l,k)) .*normalizer(cross(k,j))) <-1
    A2=acos(-1);
else
        if sum(conj(normalizer(cross(l,k)) .*normalizer(cross(k,j))))>1
            A2=0;
        else
            A2=acos(sum(conj(normalizer(cross(l,k)) .*normalizer(cross(k,j)))));
    
        end
    end

%angle 3
        if sum(normalizer(cross(k,j)) .*normalizer(cross(j,i))) <-1
    A3=acos(-1);
else
        if sum(conj(normalizer(cross(k,j)) .*normalizer(cross(j,i))))>1
            A3=0;
        else
            A3=acos(sum(conj(normalizer(cross(k,j)) .*normalizer(cross(j,i)))));
    
        end
        end
    
 %angle 3
        if sum(normalizer(cross(k,j)) .*normalizer(cross(j,i))) <-1
    A3=acos(-1);
else
        if sum(conj(normalizer(cross(k,j)) .*normalizer(cross(j,i))))>1
            A3=0;
        else
            A3=acos(sum(conj(normalizer(cross(k,j)) .*normalizer(cross(j,i)))));
    
        end
        end
    %angle 4
        if sum(normalizer(cross(j,i)) .*normalizer(cross(i,l))) <-1
    A4=acos(-1);
else
        if sum(conj(normalizer(cross(j,i)) .*normalizer(cross(i,l))))>1
            A4=0;
        else
            A4=acos(sum(conj(normalizer(cross(j,i)) .*normalizer(cross(i,l)))));
    
        end
        end
        if A1+A2+A3+A4>=0
            A=abs(2*pi-(A1+A2+A3+A4));
        else
            A=abs(-2*pi-(A1+A2+A3+A4));
        end
        if A<.000001
            A=0;
        end 
   A=s*A;     
end
function n=normalizer(x1)
if x1==0
    n=[0,0,0];
else
    n=1/sqrt(x1(1)^2+x1(2)^2+x1(3)^2)*x1;
end
end
