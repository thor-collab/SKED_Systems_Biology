classdef clsSKEDData < clsSKEDBase
    properties (SetAccess = public)
        DataType % String, e.g. 'FXGN'
        DataAssociations % cell of viable unique combination of metadata
        ExperimentID  %Integer, Experiment ID e.g. 13  
        TimeSpan %Starting Date and End Date of Experiment
        DataPrimitive %Contains DataPrimitive
    end
     
     methods
         
        %Constructor class
        %Function: clsSKEDData(oSubject,cDataType); 
        %Purpose: Construct clsSKEDData Class. 
        %Input: oSubject, clsSKEDSubject Object, cDataType, DataType
        %Output: clsSKEDDataObject 
        %Example: oData = clsSKEDData(oSubject,cDataType); 

        function o = clsSKEDData(oSubject,cDataType)
             %o.DBConnection = oSubject.DBConnection;
             o.ID = oSubject.ID; 
             o.Name = oSubject.Name; 
             o.ExperimentID = oSubject.ExperimentID;
             o.DataType = cDataType{1}; 
             o.DataAssociations = getDataAssociations(o); 

        end
        
        %Function:getTimeSpan(o,cMetaData,n)
        %Purpose: populate TimeSpan Field
        %Input: cMetaData, list of metadata terms, n, field number on the
        %data base
        %Output: clsSKEDDataObject 
        %Example: o = getTimeSpan(o,cMetaData,1); 
        
        function o = getTimeSpan(o,cMetaData,n) 
              switch n
                case 1
                   Value = 'am_value1'; 
                case 2
                   Value = 'am_value2'; 
                case 3
                   Value = 'am_value3'; 
                case 4
                   Value = 'am_value4'; 
            end
           cMetaData = cMetaData(:,1:3); 
           cMetaData = vertcat(o.DataType,cMetaData);
           numMetaData = size(cMetaData,1);

           sMetaData =[];
           for i = 1:numMetaData 
               if i > 1 
                   sMetaData = [sMetaData,' or ','(id_metadata = ',...
                       num2str(cMetaData{i,1}),')'];
               else
                   sMetaData = ['(id_metadata = ',...
                       num2str(cMetaData{i,1}),')'];
               end
           end
           
           
               sSQL = [
                   'EXEC dbo.getTimeSpan @SubjectID = ',...
                   num2str(o.ID),', @NumParameters = ',...
                   num2str(numMetaData),', @Parameters = ''',...
                   sMetaData,'''',',@ExperimentID = ',num2str(o.ExperimentID)];
               cDates = getRecordset(o,sSQL); %fetch(getDBConnection(o),sSQL);

               if isempty(cDates) == 1 
                    error('Error. Impossible MetaData Combination')
               end
               
                if strcmp(cDates{1},'null') == 1 || strcmp(cDates{2},'null') == 1 
                    error('Error. Impossible MetaData Combination')
                end
               
               o.TimeSpan = cDates; 
        end
        
        %Populate Time Series Data Primitive
        %Function:getTimeSeries(o,cMetaData,n,sDate,eDate,nDownSample,sName)
        %Purpose: Populate Time Series Data Primitive
        %Input: cMetaData, list of metadata terms, n, field number on the
        %data base, sDate, starting date, eDate, end Date, nDownSample,
        %Down sample parameter, sName, 'FXGN'or 'METABOLOMIC'
        
        %Output: clsSKEDDataObject 
        %Example: o = getTimeSeries(o,cMetaData,n,sDate,eDate,nDownSample,sName) 
        
        function o = getLowFreqTimeSeries(o,cMetaData)

               sSQL = ['begin sked.GET_FIELDS(protocol_app_id =>',num2str(cMetaData{1,1}),...
                ');end;'];
                
                
                cFields = getRecordset(o,sSQL);
   
                cFields = table2cell(cFields); 
                
                if size(cFields,1) > 100
                    idx = 0:100:size(cFields,1);
                    idx = [idx size(cFields,1)];
                    cVarName = {};
                    mData = [];
                    for i = 1:length(idx)-1
                        ia = idx(i) + 1; 
                        ib = idx(i+1); 
                        sFields = []; 
                        for j = ia:ib
                            sFields = [sFields ',' cFields{j,1}];
                        end
                        sFields = sFields(2:end);
                        
                         sSQL = ['begin get_time_series(subject_id =>',num2str(o.ID),...
                        ',protocol_app_id =>',num2str(cMetaData{1,1}),...
                        ',fields => ''',sFields,''');end;'];
                    
                        cData = getRecordset(o,sSQL);
                        cTempVarName = cData.Properties.VariableNames;
                        cVarName = horzcat(cVarName,cTempVarName(2:end));
                        cData = table2cell(cData);
                        cTempData = cData(:,2:end);
                        isString = cellfun(@ischar, cTempData);
                        cTempData(isString) = {NaN};
                        mData = [mData cell2mat(cTempData)];
                        cDates = cData(:,1); 
                    end

                else
                     sSQL = ['begin sked.get_time_series(subject_id =>',num2str(o.ID),...
                    ',protocol_app_id =>',num2str(cMetaData{1,1}),...
                    ');end;'];
                
                     cData = getRecordset(o,sSQL);
                     cVarName = cData.Properties.VariableNames;
                     cVarName = cVarName(2:end);
                     cData = table2cell(cData); 
                     cDates = cData(:,1); 
                     
                     idx = strcmp('null',cData);
                     if sum(find(idx)) > 0 
                        cData(idx) = {[NaN]};
                     end
                     mData = cell2mat(cData(:,2:end)); 
                end



              
               
                
                   mData = mData';
                   o.DataPrimitive = clsSKEDTimeSeries(cDates,cVarName,mData);



              
           
           o.Metadata = cMetaData;
           %setdbprefs('DataReturnFormat','cellarray')

        end
        
        %Function:getList(o,cMetaData,n)
        %Purpose: get data in list format
        %Input: cMetaData, list of metadata terms, n, field number on the
        %data base
        %Output: clsSKEDDataObject 
        %Example: o = getTimeSpan(o,cMetaData,1); 
        
        function [cData] = getList(o,cMetaData,n)
            numMetaData = size(cMetaData,1);
           sMetaData =[];
           for i = 1:numMetaData 
               if i > 1 
                   sMetaData = [sMetaData,' or ','(id_metadata = ',...
                       num2str(cMetaData{i,1}),')'];
               else
                   sMetaData = ['(id_metadata = ',...
                       num2str(cMetaData{i,1}),')'];
               end
           end           
           sSQL = ['EXEC dbo.getList @SubjectID = N''',...
               num2str(o.ID),''', @NumParameters = N''',...
               num2str(numMetaData),''', @Parameters = N''',...
               sMetaData,'''',',@Value = N''',...
               o.Value,'''',',@ExperimentID = ',num2str(o.ExperimentID)];
           cData = getRecordset(o,sSQL); %fetch(getDBConnection(o),sSQL);
        end
        
        %Function:getDataAssociations(o)
        %Purpose: get metadata association
        %Input: clsSKEDDataObject
        %Output: clsSKEDDataObject 
        %Example: clsSKEDData.getDataAssociations();
        
        function [cData] = getDataAssociations(o)
           sSQL = ['begin sked.get_protocols(subject_id =>',num2str(o.ID),...
                ',experiment_id => ''',o.ExperimentID,'''',...
                ');end;'];
           cData = getRecordset(o,sSQL); %fetch(getDBConnection(o),sSQL);
           cData = cData(strcmp(cData.DATA_TYPE,o.DataType),:);
        end
        
       
      
     end
end