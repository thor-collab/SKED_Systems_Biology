classdef clsSKEDExperiment < clsSKEDBase
    %------------------------------
    properties (SetAccess = public)
        Subject
        Description 
    end
    %------------------------------
    methods 
        % Constructor class
        %Function: clsSKEDExperiment(s); 
        %Purpose: Construct clsSKEDExperiment Class. 
        %Input: String - Experiment Name. 
        %Output: clsSKEDExperiment Object. 
        %Example: oExperiment = clsSKEDExperiment('E04'); 

        function o = clsSKEDExperiment(s)
            %o.DBConnection = getDBConnection(o);
            % Create instance and assign external ID
            o.ExternalID = s;
            o.Name = s;
            % getRecordset internal ID and load it in the class
            temp = getExperimentID(o);
            o.ID = s; 
            o.Description = temp.DESCRIPTION{1};
            % getRecordset subjects per experiment
            cSubjectIDs = getSubjectIDs(o); 
            for i = 1:length(cSubjectIDs)
                
                o.Subject{i} = clsSKEDSubject(cSubjectIDs(i),o);
            end
        end           
        %------------------------------
        %get metadata as an independent task
        
        function o = getMetadata(o)
           sSQL = ['begin sked.protocols(experiment_id =>''',o.Name,''');end;'];
           temp = getRecordset(o,sSQL); %fetch(getDBConnection(o),sSQL);
           o.Metadata = temp; 
        end
        %get experiment id 
        
    end
    
    methods(Access = private)
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        function cData = getExperimentID(o)
            sSQL = ['begin sked.get_experiments(experiment_id =>''',o.Name,''');end;'];
            cData = getRecordset(o,sSQL);
        end
        %get subjectids 
        function cData = getSubjectIDs(o)
            sSQL = ['begin sked.get_subjects(experiment_id =>''',o.Name,''');end;'];
            cData = getRecordset(o,sSQL);
            cData = cData.ID(:,1); 
        end
    end
        
end